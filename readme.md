## Bootstrap 4 | Configurancion con Gulp, Sass, Font Awesome y BrowserSync

Ejemplo de como configurar un proyecto profesional utilizando Bootstrap 4, el procesador de CSS3 llamado Sass, unido a BrowserSync, y utilizando el administrador de tareas Gulp.js.
Crearemos un proyecto que podrás reutilizar, cada vez que tengas que crear un proyecto nuevo utilizando Bootstrap 4.

Crear un *package.json* que nos permitirá configurar un proyecto:
~~~
$ npm init --yes
~~~

Instalar paquetes, dependencias, herramientas o bibliotecas que nos permitiran desarrollar con Bootstrap 4. Se agregarán a **node_modules**:
~~~
$ npm install bootstrap font-awesome jquery popper.js
~~~

Instalar herramientas que nos ayudará a desarrollar el proyecto pero que no estarán en el resultado final:
~~~
$ npm install -D gulp gulp-cli gulp-sass browser-sync
~~~
-D : Instalar dependencias en una sección aparte.

gulp:

gulp-sass: Convierte código SASS en código CSS.

browser-sync:

Para configurar herramientas de **Gulp**, debemos crear un archivo llamado **gulpfile.js**. Donde estarán las tareas y configuraciones que nos ayudaran a desarrollar el proyecto.

Crear la carpeta **src** donde estarán los archivos de nuestro proyecto.

Para correr las tareas hechas con **Gulp**, corremos en el terminal $ npm start, en el cual "start" se configuro previamente en el **package.json** para usar el comando **gulp**.
~~~
$ npm start
~~~